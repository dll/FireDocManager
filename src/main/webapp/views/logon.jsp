<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML>
<html>
 <head>
  <base href="<%=basePath%>">
  <meta charset="UTF-8">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>ClearForms</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="assets/css/font-awesome.min.css" />

  <style>
html {
	min-height: 100%;
	background: #C07B3A
		url('http://demo.shnayder.pro/works/sunrise/img/bg.jpg') 100% 100%
		no-repeat;
	-webkit-background-size: cover;
	background-size: cover;
	color: white;
}

.clear-form {
	max-width: 380px;
	margin: 0 auto 20px;
	padding: 15px 55px;
	background-color: #fff;
	border: 1px solid rgba(0, 0, 0, 0.1);
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: 3px;
	-webkit-box-shadow: 0px 2px 6px rgba(0, 0, 0, 0.1);
	-moz-box-shadow: 0px 2px 6px rgba(0, 0, 0, 0.1);
	box-shadow: 0px 2px 6px rgba(0, 0, 0, 0.1);
	background-color: transparent;
}
</style>

 </head>

 <body style="background-color: transparent; color: white; line-height: 2em;">
  <div class="container">
   <div style="margin-top: 35px;">
    <div class="row">
     <div class="col-md-6">
      <h1>
       Welcome to FireDoc!
      </h1>
      <hr />
      <ul>
       <li>
        更好的文档管理系统.
       </li>
       <li>
        FireDoc 支持Microsoft office（doc、docx、xls、xlsx、ppt、pptx），PDF，openoffice（odf、。。。）等文档格式.
       </li>
       <li>
        基于Lucene的文档索引构建，全文搜索更简单.
       </li>
      </ul>
     </div>
     <div class="col-md-5 col-md-offset-1">
      <div class="clear-form logon">
       <form class="form-horizontal" action="logon_logon.html" method="post">
        <div class="form-heading">
         <h3>
          登陆
         </h3>
        </div>
        <div class="form-group">
         <input type="text" class="form-control" name="username" placeholder="用户名">
        </div>
        <div class="form-group">
         <input type="password" class="form-control" name="psw" placeholder="密码">
        </div>
        <div class="form-group">

         <div class="col-sm-6">
          <label class="checkbox">
           <input type="checkbox" value="remember-me">
           记住状态
          </label>
         </div>
         <div class="col-sm-4 col-sm-offset-2">
          <button type="submit" class="btn btn-default" onclick="logon()">
           登陆
          </button>
         </div>
        </div>
        <span class="message" logon="<c:out value='${logon }'/>" style="color: red;"><c:out value="${message }"></c:out> </span>
        <div class="form-footer">
         <hr />
         <p class="center">
          <a href="javascrpit:void();" onclick="change(0)">无账号？点此注册</a>
         </p>
        </div>
       </form>
      </div>
      <div class="clear-form register">
       <form class="form-horizontal" action="logon_reg.html" method="post">
        <div class="form-heading">
         <h3>
          注册
         </h3>
        </div>
        <div class="form-group">
         <input type="text" class="form-control" name="username" placeholder="用户名">
        </div>
        <div class="form-group">
         <input type="email" class="form-control" name="email" placeholder="邮箱">
        </div>
        <div class="form-group">
         <input type="password" class="form-control" name="psw" placeholder="密码">
        </div>
        <div class="form-group">

         <div class="col-sm-4 col-sm-offset-8">
          <button type="submit" class="btn btn-default" onclick="register()">
           注册
          </button>
         </div>
        </div>
        <span class="message" logon="<c:out value='${logon}'/>" style="color: red;"><c:out value="${message }"></c:out> </span>
        <div class="form-footer">
         <hr />
         <p class="center">
          <a href="javascrpit:void();" onclick="change(1)">已有账号？点此登陆</a>
         </p>
        </div>
       </form>
      </div>
     </div>
    </div>
   </div>
  </div>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>

  <script type="text/javascript" charset="utf-8">
	function register() {
		var u = $("div[class='clear-form register'] input[name='username']")[0].value;
		var e = $("div[class='clear-form register'] input[name='email']")[0].value;
		var p = $("div[class='clear-form register'] input[name='psw']")[0].value;
		$.post("admin.html", function(data) {
		});

	}

	function logon() {
		var u = $("div[class='clear-form logon'] input[name='username']")[0].value;
		var p = $("div[class='clear-form logon'] input[name='psw']")[0].value;
		$.post("", function(data) {
		});
	}

	function change(who) {
		if (who == 0) {
			$("div[class='clear-form register']").css("display", "block");
			$("div[class='clear-form logon']").css("display", "none");
		} else if (who == 1) {
			$("div[class='clear-form register']").css("display", "none");
			$("div[class='clear-form logon']").css("display", "block");
		}
	}

	function getQueryString(name) {
		var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
		var r = window.location.search.substr(1).match(reg);
		if (r != null)
			return unescape(r[2]);
		return null;
	}
	$(function() {
		var zt = getQueryString("logon");
		if (zt == null) {
			zt = $(".message").attr("logon");
		}
		if (zt == null || zt == "" || zt == "logon") {
			change(1);
		} else {
			change(0);
		}
	});
</script>

 </body>

</html>