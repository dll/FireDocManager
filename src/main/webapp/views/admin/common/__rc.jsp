<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"
	contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:forEach var="role" items="${roletree }">
	<div class="tree-folder" style="display: block;">
		<div class="tree-folder-header">
			<i class="red"></i>
			<div class="tree-folder-name" cateid='<c:out value="${role.id}" />'
				parentid='<c:out value="${role.parent}" />'>
				<label>
					<input name="roles" type="checkbox" class="ace" value="<c:out value='${role.id}' />" />
					<span class="lbl"> <c:out value="${role.title}" />
					</span>
				</label>

			</div>
		</div>
		<div class="tree-folder-content" style="display: block;">
			<c:if test="${fn:length(role.childs) > 0}">
				<!-- 如果有childen -->
				<c:set var="roletree" value="${role.childs}" scope="request" />
				<!-- 注意此处，子列表覆盖treeList，在request作用域 -->
				<c:import url="common/__rc.jsp" />
				<!-- 这就是递归了 -->
			</c:if>

		</div>

	</div>
</c:forEach>