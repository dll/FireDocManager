<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="smvc" value="${pageContext.request.contextPath}" />
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>


<jsp:include page="common/admin-common-head.jsp"></jsp:include>
<jsp:include page="common/admin-common-sidebar.jsp"></jsp:include>
<div class="page-header">
 <h1>
  文档管理
  <small> <i class="icon-double-angle-right"></i> 快速文档添加 </small>
 </h1>
</div>
<!-- /.page-header -->
<div class="row">
 <div class="col-xs-12">
  <div class="row">
   <div class="col-sm-5">
    这里为上传文档处
    <div id="bootstrapped-fine-uploader"></div>


   </div>
   <div class="col-sm-5">
    <div class="row" id="doc-callback-item">
     <!-- js 自动生成模板  -->
    </div>
   </div>
  </div>
 </div>
</div>
<!-- Fine Uploader JS
====================================================================== -->
<link rel="stylesheet" href="<c:out value='${smvc }'/>/assets/css/fineuploader.css" />
<script src="<c:out value='${smvc }'/>/assets/js/all.fineuploader-4.4.0.min.js"></script>
<script src="<c:out value='${smvc}'/>/assets/layer/layer.min.js"></script>
<script>
	$(document).ready(function() {
		initMyRole();
		var uploader = $("#bootstrapped-fine-uploader").fineUploader({
			element : $('#bootstrapped-fine-uploader'),
			debug : true,
			request : {
				endpoint : $("html").attr("basepath")+'/file/upload/upload.json'
			},
			template : 'qq-template-bootstrap',
			classes : {
				success : 'alert alert-success',
				fail : 'alert alert-error'
			},
			validation : {
				allowedExtensions : [ 'doc', 'docx', 'txt','ppt','pptx','xls','xlsx','pdf' ],
				sizeLimit : 3145728
			// 50 kB = 50 * 1024 bytes
			// 3M =3*1024*1024 bytes
			},
			messages:{
				typeError:'{file} 不是允许的格式. 支持文件类型列表: {extensions}.',
				sizeError:'文件 {file} 太大, 最大支持上传大小为： {sizeLimit}.'
			},
			callbacks : {
				onComplete : function(id, fileName, responseJSON) {
					if (responseJSON.success) {
						var docid = responseJSON.docid;
						var docname = responseJSON.docname;
						var appstring = $("#doc-form-template").html().replace("docid-value", docid).replace("docname-value", docname);
						$("#doc-callback-item").append(appstring);
						//请求parser文档
						///utils/docParser_{ID}.json
						parser(docid);
					}
				}
			}
		});
	});
	function parser(docid) {
		var contextPath = $("html").attr("basepath");
		$.post(contextPath + "/utils/docParser_" + docid + ".json", function(data) {
		});

	}
	function initMyRole() {
		var len = $("#form-roles-select-template").find("option").length;
		if (len == 0) {
			var contextPath = $("html").attr("basepath");
			$.post(contextPath + "/admin/role-u.html", function(data) {
				//解析数组
				$.each(data, function(i, item) {
					$("#form-roles-select-template").append("<option value='"+item.id+"'>" + item.title + "</option>");
				});
			}, "json");
		}
	}
	function updateDocMeta(event) {
		var ii = layer.load();
		var par = $(event).closest('div[role=form]');
		var did = $(par).find("input[name=docid]").attr("value");
		var docname = $(par).find("input[name=doaname]").val();
		var cate = $(par).find("select[name=cate]").val();
		var role = $(par).find("select[name=role]").val();
		var contextPath = $("html").attr("basepath");
		$.post(contextPath + "/admin/doc_updateMeta.html", {
			docId : did,
			docname : docname,
			cate : cate,
			role : role
		}, function(data) {
			layer.close(ii);
			//layer.msg('更新成功？' + data.success, 1, -1); //1秒后自动关闭，-1代表不显示图标

		}, "json");
	}
</script>
<!-- Fine Uploader template
====================================================================== -->
<script type="text/template" id="qq-template-bootstrap">
  <div class="qq-uploader-selector qq-uploader span12">
    <div class="qq-upload-drop-area-selector qq-upload-drop-area span12" qq-hide-dropzone>
      <span>Drop files here to upload</span>
    </div>
    <div class="qq-upload-button-selector qq-upload-button btn btn-success" style="width: 100%;">
      <div><i class="icon-upload icon-white"></i> Test me now and upload a file</div>
    </div>
    <span class="qq-drop-processing-selector qq-drop-processing">
      <span>Processing dropped files...</span>
      <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
    </span>
    <ul class="qq-upload-list-selector qq-upload-list" style="margin-top: 10px; text-align: center;">
      <li>
        <div class="qq-progress-bar-container-selector">
          <div class="qq-progress-bar-selector qq-progress-bar"></div>
        </div>
        <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
        <span class="qq-upload-file-selector qq-upload-file"></span>
        <span class="qq-upload-size-selector qq-upload-size"></span>
        <a class="qq-upload-cancel-selector qq-upload-cancel" href="#">Cancel</a>
        <span class="qq-upload-status-text-selector qq-upload-status-text"></span>
      </li>
    </ul>
  </div>
</script>


<div type="text/template" id="doc-form-template" style="display: none;">
 <div class="form-horizontal" role="form" style="display: block;">
  <input type="text" name="docid" hidden="hidden" value="docid-value">
  <div class="col-md-9">
   <div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">
     名称
    </label>
    <div class="col-sm-9">
     <input type="text" id="form-field-1" name="doaname" value="docname-value" placeholder="请输入修改名称" class="col-xs-12 col-sm-12" />
    </div>
   </div>
   <div class="space-4"></div>
   <div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">
     分类
    </label>

    <div class="col-sm-9">

     <select class="form-control" name="cate" id="form-cates-select-template">
      <c:forEach var="c" items="${allcates }">

       <option value="<c:out value="${c.id}" />">
        <c:out value="${c.title}" />
       </option>
      </c:forEach>
     </select>


    </div>
   </div>
   <div class="space-4"></div>
   <div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">
     权限组
    </label>
    <div class="col-sm-9">
     <select class="form-control" name="role" id="form-roles-select-template">

     </select>
    </div>
   </div>
   <div class="space-4"></div>
   <hr>
  </div>
  <div class="col-md-2 col-md-offset-1">
   <button class="btn btn-success" id="updatedocmetabtn" onclick="updateDocMeta(this)">
    提交
   </button>
  </div>
 </div>
 <hr>
</div>
<jsp:include page="common/admin-common-footer.jsp"></jsp:include>