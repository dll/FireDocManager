<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"
	contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="smvc" value="${pageContext.request.contextPath}" />
<jsp:include page="common/admin-common-head.jsp"></jsp:include>
<jsp:include page="common/admin-common-sidebar.jsp"></jsp:include>
<div class="page-header">
	<h1>
		用户管理
		<small> <i class="icon-double-angle-right"></i> 快速添加用户 </small>
	</h1>
</div>
<!-- /.page-header -->
<div class="row">
	<div class="col-xs-12">
		<div class="row">
			<form class="form-horizontal" role="form"
				action="<c:out value='${smvc }'/>/admin/adduser.html">
				<div class="col-sm-4">
					<div class="space-4"></div>
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right"
							for="form-field-tags">

						</label>

						<div class="col-sm-9">
							<div class="widget-header header-color-green2">
								<h4 class="lighter smaller">
									选择用户所有权限组（多选）
								</h4>
							</div>
							<div class="widget-body">
								<div class="widget-main padding-8">
									<div id="tree2" class="tree tree-unselectable">
										<c:import url="common/__rc.jsp"></c:import>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<br />
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right"
							for="form-field-1">
							用戶名
						</label>

						<div class="col-sm-9">
							<input type="text" name="userName" id="form-field-1"
								placeholder="用户名" class="col-xs-12 col-sm-12">
						</div>
					</div>
					<div class="space-4"></div>
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right"
							for="form-field-1">
							邮箱
						</label>

						<div class="col-sm-9">
							<input type="text" name="email" id="form-field-1"
								placeholder="邮箱" class="col-xs-12 col-sm-12">
						</div>
					</div>


					<div class="space-4"></div>

					<div class="clearfix">
						<div class="col-md-offset-3 col-md-9">
							<button class="btn btn-info" type="button submit">
								<i class="icon-ok bigger-110"></i> 提交
							</button>

							&nbsp; &nbsp; &nbsp;
							<button class="btn" type="reset">
								<i class="icon-undo bigger-110"></i> 重置
							</button>
						</div>
					</div>

				</div>

			</form>
		</div>
	</div>
</div>
<jsp:include page="common/admin-common-footer.jsp"></jsp:include>