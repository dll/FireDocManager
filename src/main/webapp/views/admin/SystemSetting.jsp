<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="smvc" value="${pageContext.request.contextPath}" />

<jsp:include page="common/admin-common-head.jsp"></jsp:include>
<jsp:include page="common/admin-common-sidebar.jsp"></jsp:include>
<div class="page-header">
 <h1>
  系统设置
  <small> <i class="icon-double-angle-right"></i> 系统设置 </small>
 </h1>
</div>
<!-- /.page-header -->
<div class="row">
 <div class="col-xs-12">
  <div class="row">
   <div class="form-horizontal" role="form" name="uedit_user_simple">

    <div class="col-sm-6">
     <br />
     <div class="form-group">
      <label class="col-sm-3 control-label no-padding-right" for="form-field-1">
       文档服务器地址
      </label>

      <div class="col-sm-9">
       <input type="text" name="file_server_prefix" id="form-field-1" class="col-xs-12 col-sm-12" value="<c:out value='${file_server_prefix}'/>">
      </div>
     </div>
     <div class="space-4"></div>
     <div class="form-group">
      <label class="col-sm-3 control-label no-padding-right" for="form-field-1">
       索引路径
      </label>

      <div class="col-sm-9">
       <input type="text" name="idx_path" id="form-field-1" class="col-xs-12 col-sm-12" value="<c:out value='${idx_path}'/>" />

      </div>
     </div>
     <div class="space-4"></div>
     <div class="form-group">
      <label class="col-sm-3 control-label no-padding-right" for="form-field-1">
       文档路径
      </label>

      <div class="col-sm-9">
       <input type="text" name="opload_filePath" id="form-field-1" class="col-xs-12 col-sm-12" value="<c:out value='${opload_filePath}'/>" />
      </div>
     </div>


     <div class="space-4"></div>

     <div class="clearfix">
      <div class="col-md-offset-3 col-md-9">
       <button class="btn btn-info" type="button submit" onclick="updateSystemSetting(this);">
        <i class="icon-ok bigger-110"></i> 提交
       </button>
       &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
       <span style="color: red;" class="message"></span>
      </div>
     </div>

    </div>

   </div>
  </div>
  <hr>
  <div class="row">
   <div class="chongjiansuoyin">
    <div class="col-md-4">
     <div class="form-group">
      <label class="col-sm-3 control-label no-padding-right">
       重建索引
      </label>
      <div class="col-sm-9">
       <input type="text" name="chongjiansuoyin" class="col-xs-12 col-sm-12">

      </div>
      <div class="col-sm-12 col-sm-offset-3">
       注意：重建索引为非常规操作，如果确实需要重建索引，将上面文本框内填入
       <span style="color: red;">ok</span>，并点击下方重建索引按钮
      </div>
     </div>
    </div>
    <div class="col-sm-4 col-sm-offset-2">
     <button class="btn btn-info" type="button submit" onclick="reIndexAll(this);">
      <i class="icon-ok bigger-110"></i> 重建索引
     </button>
     &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
     <span style="color: red;" class="message2"></span>
    </div>
   </div>
  </div>
 </div>
</div>
<script src="<c:out value='${smvc}'/>/assets/layer/layer.min.js"></script>
<script type="text/javascript">
	function updateSystemSetting(event) {
		var ii = layer.load();
		var par = $(event).closest('div[name=uedit_user_simple]');
		var file_server_prefix = $(par).find("input[name=file_server_prefix]").attr("value");
		var idx_path = $(par).find("input[name=idx_path]").val();
		var opload_filePath = $(par).find("input[name=opload_filePath]").val();

		var contextPath = $("html").attr("basepath");
		$.post(contextPath + "/admin/systemsetting.html", {
			file_server_prefix : file_server_prefix,
			idx_path : idx_path,
			opload_filePath : opload_filePath
		}, function(data) {
			layer.close(ii);
			$(".message").text(data.success);

		}, "json");
	}
	function reIndexAll(event) {
		var ii = layer.load();
		var par = $(event).closest('div[class=chongjiansuoyin]');
		var chongjiansuoyin = $(par).find("input[name=chongjiansuoyin]").val();
		if (chongjiansuoyin != "ok") {
			layer.close(ii);
			$(".message2").text("必须在输入框中填入ok，才可以重建索引");
		} else {
			var contextPath = $("html").attr("basepath");
			$.post(contextPath + "/admin/doc_reindexall.html", function(data) {
				layer.close(ii);
				$(".message").text(data.success);
			}, "json");
		}
	}
</script>
<jsp:include page="common/admin-common-footer.jsp"></jsp:include>