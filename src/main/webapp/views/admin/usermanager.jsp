<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="smvc" value="${pageContext.request.contextPath}" />
<jsp:include page="common/admin-common-head.jsp"></jsp:include>
<jsp:include page="common/admin-common-sidebar.jsp"></jsp:include>
<div class="page-header">
 <h1>
  注册用户管理
  <small> <i class="icon-double-angle-right"></i> 用户管理 </small>
 </h1>
</div>
<div class="dataTables_wrapper" role="grid">
 <div class="row" style="height: 43px;">
  <div class="col-sm-6">
   <div id="sample-table-2_length" class="dataTables_length">
    <label>
     共
     <c:out value="${fn:length(users) }" />
     条记录
    </label>
   </div>
  </div>
  <div class="col-sm-6">
   <div class="dataTables_filter" id="sample-table-2_filter">
    <label>
     搜索:
     <input type="text" aria-controls="sample-table-2">
    </label>
   </div>
  </div>
 </div>

 <table class="table table-striped table-bordered table-hover dataTable">
  <thead>
   <tr role="row">
    <th class="center sorting_disabled" role="columnheader" rowspan="1" colspan="1" style="width: 57px;" aria-label="">
     <label>
      <input type="checkbox" class="ace">
      <span class="lbl"></span>
     </label>
    </th>
    <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="sample-table-2" rowspan="1" colspan="1" style="width: 162px;" aria-sort="ascending">
     ID
    </th>
    <th class="sorting_desc" role="columnheader" tabindex="0" aria-controls="sample-table-2" rowspan="1" colspan="1" style="width: 162px;" aria-sort="ascending">
     用户名
    </th>
    <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="sample-table-2" rowspan="1" colspan="1" style="width: 162px;" aria-sort="ascending">
     邮箱
    </th>
    <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="sample-table-2" rowspan="1" colspan="1" style="width: 162px;" aria-sort="ascending">
     注册时间
    </th>
    <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="sample-table-2" rowspan="1" colspan="1" style="width: 162px;" aria-sort="ascending">
     上传文档数量
    </th>
    <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="sample-table-2" rowspan="1" colspan="1" style="width: 162px;" aria-sort="ascending">
     权限组
    </th>
    <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="sample-table-2" rowspan="1" colspan="1" style="width: 162px;" aria-sort="ascending">
     操作
    </th>
   </tr>
  </thead>


  <c:forEach var="u" items="${users }">
   <tr>
    <td class="center  sorting_1">
     <label>
      <input type="checkbox" class="ace">
      <span class="lbl"></span>
     </label>
    </td>
    <td>
     <c:out value="${u.ID }"></c:out>
    </td>
    <td>
     <c:out value="${u.userName }"></c:out>
    </td>
    <td>
     <c:out value="${u.email }"></c:out>
    </td>
    <td>
     <c:out value="${u.joined }"></c:out>
    </td>
    <td>
     <c:out value="${u.docNum }"></c:out>
    </td>
    <td>
     <c:out value="${u.quanxian }"></c:out>
    </td>
    <td>
     <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
      <a class="green" href="#"> <i duser="<c:out value='${u.userName }'/>" class="user_change_authority icon-group bigger-130"></i> </a>
      <a class="red" href="#"> <i duser="<c:out value='${u.userName }'/>" class="icon-edit  bigger-130"></i> </a>

      <a class="red" href="#"> <i duser="<c:out value='${u.userName }'/>" class="user_delete icon-trash bigger-130"></i> </a>
     </div>
    </td>
   </tr>
  </c:forEach>



 </table>



</div>

<script src="<c:out value='${smvc}'/>/assets/layer/layer.js"></script>

<script>
	$('.user_delete').on('click', function(event) {
		var ii = layer.load();
		var duser = $(this).attr("duser");
		var par = $(this).closest('tr');
		var contextPath = $("html").attr("basepath");
		$.post(contextPath + "/admin/user_del.html", {
			username : duser
		}, function(data) {
			layer.close(ii);
			console.log(data.success);
			if (data.success) {
				$(par).remove();
			}
		}, "json");

	});
	$('.user_change_authority').on('click', function() {
		var duser = $(this).attr("duser");
		var i = $.layer({
			type : 1,
			title : false,
			fix : false,
			closeBtn : false,
			shadeClose : true,
			border : [ 0 ],
			offset : [ '200px', '' ],
			area : [ '620px', 'auto' ],
			page : {
				html : '<div id="user_change_authority_roles" style="background-color: white;padding: 15px;"></div>'
			},
			success : function() {
				layer.shift('top', 200);
			}
		});

		initAuthority(duser);
	});
	function initAuthority(username) {
		var ii = layer.load();
		var contextPath = $("html").attr("basepath");
		$.post(contextPath + "/admin/role-u-all.html", {
			username : username
		}, function(data) {

			//解析数组
			$.each(data, function(i, item) {
				var id = item.id;
				var title = item.title;
				var checked = item.checked;
				if (checked == 1) {
					$('#user_change_authority_roles').append('<lable><input type="checkbox" class="authority_checkbox" un="'+username+'" rid="'+id+'" checked>' + title + '</lable>');
				} else {
					$('#user_change_authority_roles').append('<lable><input type="checkbox" class="authority_checkbox" un="'+username+'" rid="'+id+'">' + title + '</lable>');
				}

			});

			layer.close(ii);
		}, "json");

	}

	$('.authority_checkbox').live('click', function() {
		
		var ii = layer.load();
		var checked = $(this).is(':checked');
		var id = $(this).attr("rid");
		var username = $(this).attr("un");
		console.log(checked + "::" + id);
		var contextPath = $("html").attr("basepath");
		$.post(contextPath + "/admin/user_authority_update.html", {
			add : checked,
			roleid : id,
			username : username
		}, function(data) {
			layer.close(ii);
			if (checked) {
				
			} else {
				layer.msg('取消权限成功？' + data.success, 1, -1); //2秒后自动关闭，-1代表不显示图标
			}
		}, "json");
	});
</script>

<jsp:include page="common/admin-common-footer.jsp"></jsp:include>