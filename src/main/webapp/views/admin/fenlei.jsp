<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"
	contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="smvc" value="${pageContext.request.contextPath}" />
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="common/admin-common-head.jsp"></jsp:include>
<jsp:include page="common/admin-common-sidebar.jsp"></jsp:include>
<div class="page-header">
	<h1>
		分类管理
		<small> <i class="icon-double-angle-right"></i> 快速文档分类管理 </small>
	</h1>
</div>
<!-- /.page-header -->
<div class="row">
	<div class="col-xs-12">
		<div class="row">
			<br />
			<div class="col-sm-5">
				<div class="widget-box">
					<div class="widget-header header-color-green2">
						<h4 class="lighter smaller">
							分类树
						</h4>
					</div>
					<div class="widget-body">
						<div class="widget-main padding-8">
							<div id="tree2" class="tree tree-unselectable">
								<c:import url="common/__.jsp"></c:import>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
			<form class="form-horizontal" role="form"
					action="<c:out value='${smvc }'/>/admin/addcate.html">


					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right"
							for="form-field-1">
							父类节点
						</label>

						<div class="col-sm-9">
							<select name="parent" class="form-control"
								id="form-field-select-parent">
								<option value="-1">
									无根目录
								</option>
								<c:forEach items="${catelist }" var="cl">
									<option value="<c:out value="${cl.id }"/>">
										<c:out value="${cl.title }" />
									</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="space-4"></div>
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right"
							for="form-field-1">
							ID
						</label>

						<div class="col-sm-9">
							<input type="text" name="id" id="form-field-id" placeholder="ID"
								class="col-xs-12 col-sm-12" readonly>
						</div>
					</div>
					<div class="space-4"></div>
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right"
							for="form-field-1">
							名称
						</label>

						<div class="col-sm-9">
							<input type="text" name="title" id="form-field-title"
								placeholder="名称" class="col-xs-12 col-sm-12">
						</div>
					</div>
					<div class="space-4"></div>
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right"
							for="form-field-1">
							操作
						</label>

						<div class="col-sm-9">
							<div class="control-group">
								<label>
									<input name="option" type="radio" class="ace" value="0" />
									<span class="lbl">新建</span>
								</label>
								<label>
									<input name="option" type="radio" class="ace" checked="checked" value="1" />
									<span class="lbl"> 修改</span>
								</label>
								<label>
									<input name="option" type="radio" class="ace"  value="2" />
									<span class="lbl"> 删除</span>
								</label>

							</div>
						</div>
					</div>
					<div class="space-4"></div>
					<div class="clearfix">
						<div class="col-md-offset-3 col-md-9">
							<button class="btn btn-info" type="button submit">
								<i class="icon-ok bigger-110"></i> 提交
							</button>

							&nbsp; &nbsp; &nbsp;
							<button class="btn" type="reset">
								<i class="icon-undo bigger-110"></i> 重置
							</button>
						</div>
					</div>
				</form>

				
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('.tree-folder-header').click(function() {
			var c = $(this).find(".tree-folder-name");
			var title = c.text().replace(/(^\s*)|(\s*$)/g, "");
			var cateid = c.attr("cateid");
			var parentid = c.attr("parentid");
			parentid = parentid == null ? -1 : parentid;
			console.log(parentid);
			$("#form-field-select-parent").val(parentid);
			$("#form-field-id").val(cateid);
			$("#form-field-title").val(title);
		});
	});
</script>
<jsp:include page="common/admin-common-footer.jsp"></jsp:include>