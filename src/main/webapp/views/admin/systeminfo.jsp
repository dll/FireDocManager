<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"
	contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<c:set var="smvc" value="${pageContext.request.contextPath}" />
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="common/admin-common-head.jsp"></jsp:include>
<jsp:include page="common/admin-common-sidebar.jsp"></jsp:include>
<iframe src="<c:out value="${smvc }"/>/admin/environment.html" width="100%" height="500px" style="border:none;"></iframe>
<jsp:include page="common/admin-common-footer.jsp"></jsp:include>