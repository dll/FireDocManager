<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="smvc" value="${pageContext.request.contextPath}" />
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<jsp:include page="../common/admin-common-head.jsp"></jsp:include>
<jsp:include page="../common/admin-common-sidebar.jsp"></jsp:include>
<div class="page-header">
 <h1>
  用户管理
  <small> <i class="icon-double-angle-right"></i> 修改个人信息 </small>
 </h1>
</div>
<!-- /.page-header -->
<div class="row">
 <div class="col-xs-12">
  <div class="row">
   <div class="form-horizontal" role="form" name="uedit_user_simple">

    <div class="col-sm-4">
     <br />
     <div class="form-group">
      <label class="col-sm-3 control-label no-padding-right" for="form-field-1">
       登陆账号
      </label>

      <div class="col-sm-9">
       <input type="text" name="username" id="form-field-1" placeholder="用户名" class="col-xs-12 col-sm-12" value="<c:out value='${u.userName }'/>" readonly>
      </div>
     </div>
     <div class="space-4"></div>
     <div class="form-group">
      <label class="col-sm-3 control-label no-padding-right" for="form-field-1">
       邮箱
      </label>

      <div class="col-sm-9">
       <input type="text" name="email" id="form-field-1" placeholder="邮箱" class="col-xs-12 col-sm-12" value="<c:out value='${u.email }'/>">
      </div>
     </div>
     <div class="space-4"></div>
     <div class="form-group">
      <label class="col-sm-3 control-label no-padding-right" for="form-field-1">
       昵称
      </label>

      <div class="col-sm-9">
       <input type="text" name="nickname" id="form-field-1" placeholder="邮箱" class="col-xs-12 col-sm-12" value="<c:out value='${u.email }'/>">
      </div>
     </div>
     <div class="space-4"></div>
     <div class="form-group">
      <label class="col-sm-3 control-label no-padding-right" for="form-field-1">
       出生日期
      </label>

      <div class="col-sm-9">
       <select name="y">
        <option value="0">
         --
        </option>
        <option value="2001">
         2001
        </option>
        <option value="2000">
         2000
        </option>
        <option value="1999">
         1999
        </option>
        <option value="1998">
         1998
        </option>
        <option value="1997">
         1997
        </option>
        <option value="1996">
         1996
        </option>
        <option value="1995">
         1995
        </option>
        <option value="1994">
         1994
        </option>
        <option value="1993">
         1993
        </option>
        <option value="1992">
         1992
        </option>
        <option value="1991">
         1991
        </option>
        <option value="1990">
         1990
        </option>
        <option value="1989">
         1989
        </option>
        <option value="1988">
         1988
        </option>
        <option value="1987">
         1987
        </option>
        <option value="1986">
         1986
        </option>
        <option value="1985">
         1985
        </option>
        <option value="1984">
         1984
        </option>
        <option value="1983">
         1983
        </option>
        <option value="1982">
         1982
        </option>
        <option value="1981">
         1981
        </option>
        <option value="1980">
         1980
        </option>
        <option value="1979">
         1979
        </option>
        <option value="1978">
         1978
        </option>
        <option value="1977">
         1977
        </option>
        <option value="1976">
         1976
        </option>
        <option value="1975">
         1975
        </option>
        <option value="1974">
         1974
        </option>
        <option value="1973">
         1973
        </option>
        <option value="1972">
         1972
        </option>
        <option value="1971">
         1971
        </option>
        <option value="1970">
         1970
        </option>
        <option value="1969">
         1969
        </option>
        <option value="1968">
         1968
        </option>
        <option value="1967">
         1967
        </option>
        <option value="1966">
         1966
        </option>
        <option value="1965">
         1965
        </option>
        <option value="1964">
         1964
        </option>
        <option value="1963">
         1963
        </option>
        <option value="1962">
         1962
        </option>
        <option value="1961">
         1961
        </option>
        <option value="1960">
         1960
        </option>
        <option value="1959">
         1959
        </option>
        <option value="1958">
         1958
        </option>
        <option value="1957">
         1957
        </option>
        <option value="1956">
         1956
        </option>
        <option value="1955">
         1955
        </option>
        <option value="1954">
         1954
        </option>
        <option value="1953">
         1953
        </option>
        <option value="1952">
         1952
        </option>
        <option value="1951">
         1951
        </option>
        <option value="1950">
         1950
        </option>
       </select>
       年
       <select name="m">
        <option value="0">
         --
        </option>
        <option value="1">
         1
        </option>
        <option value="2">
         2
        </option>
        <option value="3">
         3
        </option>
        <option value="4">
         4
        </option>
        <option value="5">
         5
        </option>
        <option value="6">
         6
        </option>
        <option value="7">
         7
        </option>
        <option value="8">
         8
        </option>
        <option value="9">
         9
        </option>
        <option value="10">
         10
        </option>
        <option value="11">
         11
        </option>
        <option value="12">
         12
        </option>
       </select>
       月
       <select name="d">
        <option value="0">
         --
        </option>
        <option value="1">
         1
        </option>
        <option value="2">
         2
        </option>
        <option value="3">
         3
        </option>
        <option value="4">
         4
        </option>
        <option value="5">
         5
        </option>
        <option value="6">
         6
        </option>
        <option value="7">
         7
        </option>
        <option value="8">
         8
        </option>
        <option value="9">
         9
        </option>
        <option value="10">
         10
        </option>
        <option value="11">
         11
        </option>
        <option value="12">
         12
        </option>
        <option value="13">
         13
        </option>
        <option value="14">
         14
        </option>
        <option value="15">
         15
        </option>
        <option value="16">
         16
        </option>
        <option value="17">
         17
        </option>
        <option value="18">
         18
        </option>
        <option value="19">
         19
        </option>
        <option value="20">
         20
        </option>
        <option value="21">
         21
        </option>
        <option value="22">
         22
        </option>
        <option value="23">
         23
        </option>
        <option value="24">
         24
        </option>
        <option value="25">
         25
        </option>
        <option value="26">
         26
        </option>
        <option value="27">
         27
        </option>
        <option value="28">
         28
        </option>
        <option value="29">
         29
        </option>
        <option value="30">
         30
        </option>
        <option value="31">
         31
        </option>
       </select>
       日
      </div>
     </div>
     <div class="space-4"></div>
     <div class="form-group">
      <label class="col-sm-3 control-label no-padding-right" for="form-field-1">
       居住地区
      </label>

      <div class="col-sm-9">
       <select onchange="showcity(this.value, document.getElementById('userCity'));" name="province" id="userProvince">
        <option value="">
         --请选择省份--
        </option>
        <option value="北京">
         北京
        </option>
        <option value="上海">
         上海
        </option>
        <option value="广东">
         广东
        </option>
        <option value="江苏">
         江苏
        </option>
        <option value="浙江">
         浙江
        </option>
        <option value="重庆">
         重庆
        </option>
        <option value="安徽">
         安徽
        </option>
        <option value="福建">
         福建
        </option>
        <option value="甘肃">
         甘肃
        </option>
        <option value="广西">
         广西
        </option>
        <option value="贵州">
         贵州
        </option>
        <option value="海南">
         海南
        </option>
        <option value="河北">
         河北
        </option>
        <option value="黑龙江">
         黑龙江
        </option>
        <option value="河南">
         河南
        </option>
        <option value="湖北">
         湖北
        </option>
        <option value="湖南">
         湖南
        </option>
        <option value="江西">
         江西
        </option>
        <option value="吉林">
         吉林
        </option>
        <option value="辽宁">
         辽宁
        </option>
        <option value="内蒙古">
         内蒙古
        </option>
        <option value="宁夏">
         宁夏
        </option>
        <option value="青海">
         青海
        </option>
        <option value="山东">
         山东
        </option>
        <option value="山西">
         山西
        </option>
        <option value="陕西">
         陕西
        </option>
        <option value="四川">
         四川
        </option>
        <option value="天津">
         天津
        </option>
        <option value="新疆">
         新疆
        </option>
        <option value="西藏">
         西藏
        </option>
        <option value="云南">
         云南
        </option>
        <option value="香港">
         香港特别行政区
        </option>
        <option value="澳门">
         澳门特别行政区
        </option>
        <option value="台湾">
         台湾
        </option>
        <option value="海外">
         海外
        </option>
       </select>
       <select name="city" id="userCity">
        <option value="广州">
         广州
        </option>
        <option value="深圳">
         深圳
        </option>
        <option value="珠海">
         珠海
        </option>
        <option value="东莞">
         东莞
        </option>
        <option value="中山">
         中山
        </option>
        <option value="佛山">
         佛山
        </option>
        <option value="惠州">
         惠州
        </option>
        <option value="河源">
         河源
        </option>
        <option value="潮州">
         潮州
        </option>
        <option value="江门">
         江门
        </option>
        <option value="揭阳">
         揭阳
        </option>
        <option value="茂名">
         茂名
        </option>
        <option value="梅州">
         梅州
        </option>
        <option value="清远">
         清远
        </option>
        <option value="汕头">
         汕头
        </option>
        <option value="汕尾">
         汕尾
        </option>
        <option value="韶关">
         韶关
        </option>
        <option value="顺德">
         顺德
        </option>
        <option value="阳江">
         阳江
        </option>
        <option value="云浮">
         云浮
        </option>
        <option value="湛江">
         湛江
        </option>
        <option value="肇庆">
         肇庆
        </option>
       </select>
       <script src="http://my.oschina.net/js/getcity.js"></script>
      </div>
     </div>
     <div class="space-4"></div>
     <div class="form-group">
      <label class="col-sm-3 control-label no-padding-right" for="form-field-1">
       性别
      </label>

      <div class="col-sm-9">
       <input type="radio" name="gender" value="1" checked="">
       男
       <input type="radio" name="gender" value="2">
       女
      </div>
     </div>

     <div class="space-4"></div>

     <div class="clearfix">
      <div class="col-md-offset-3 col-md-9">
       <button class="btn btn-info" type="button submit" onclick="updateDocMeta(this);">
        <i class="icon-ok bigger-110"></i> 提交
       </button>
       &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
       <span style="color: red;" class="message"></span>
      </div>
     </div>

    </div>

   </div>
  </div>
 </div>
</div>
<script src="<c:out value='${smvc}'/>/assets/layer/layer.min.js"></script>
<script type="text/javascript">
	function updateDocMeta(event) {
		var ii = layer.load();
		var par = $(event).closest('div[name=uedit_user_simple]');
		var username = $(par).find("input[name=username]").attr("value");
		var email = $(par).find("input[name=email]").val();
		var nickname = $(par).find("input[name=nickname]").val();
		var y = $(par).find("select[name=y]").val();
		var m = $(par).find("select[name=m]").val();
		var d = $(par).find("select[name=d]").val();
		var province = $(par).find("select[name=province]").val();
		var city = $(par).find("select[name=city]").val();
		var gender = $(par).find("input[name=gender]:checked").val();
		var contextPath = $("html").attr("basepath");
		$.post(contextPath + "/admin/user_edit_simple.html", {
			username : username,
			email : email,
			nickname : nickname,
			y : y,
			m : m,
			d : d,
			province : province,
			city : city,
			gender : gender
		}, function(data) {
			layer.close(ii);
			$(".message").text(data.success);
			

		}, "json");
	}
</script>
<jsp:include page="../common/admin-common-footer.jsp"></jsp:include>