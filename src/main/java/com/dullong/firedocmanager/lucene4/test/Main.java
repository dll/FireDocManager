package com.dullong.firedocmanager.lucene4.test;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.dullong.firedocmanager.bean.Document;
import com.dullong.firedocmanager.lucene4.LuceneIndexService;
import com.dullong.firedocmanager.lucene4.LuceneSearchService;
import com.dullong.firedocmanager.lucene4.domain.LDocument;
import com.dullong.firedocmanager.lucene4.domain.QDocument;
import com.dullong.firedocmanager.util.ConnectionUtil;
import com.mysema.query.lucene.LuceneExpressions;
import com.mysema.query.lucene.LuceneQuery;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.Visitor;
import com.mysema.query.types.expr.BooleanExpression;

public class Main {

	public static void main(String[] s) throws SQLException, IOException,
			ClassNotFoundException {
		Connection conn = ConnectionUtil.getConnection();
		PreparedStatement ps = conn.prepareStatement("select * from document");
		ResultSet rs = ps.executeQuery();
		List<LDocument> list = new ArrayList<LDocument>();
		while (rs.next()) {
			LDocument document = new LDocument();
			document.setBody(rs.getString("body"));
			document.setCate(rs.getInt("cate"));
			document.setId(rs.getLong("id"));
			document.setPage(rs.getInt("page"));
			document.setRoleid(rs.getInt("roleid"));
			document.setTitle(rs.getString("title"));
			document.setType(rs.getString("type"));
			document.setUploadtime(rs.getLong("uploadtime"));
			list.add(document);
		}
		System.out.println(list.size());
		
		LuceneIndexService luceneIndexService = new LuceneIndexService();
		luceneIndexService.deleteAll();
		System.out.println("--------------对象创建成功");
		 luceneIndexService.add(list);
		System.out.println("----------------索引创建成功");
		 luceneIndexService.optimize();
		System.out.println("----------------索引关闭完结");

		 QDocument doc = new QDocument("doc");
		
		 LuceneQuery query = new LuceneQuery(
		 luceneIndexService.getIndexSearcher());
		
		 List<org.apache.lucene.document.Document> list2 = query.where(
		 doc.ID.eq(5l),null).list();
		
		 System.out.println(query.toString());
		 System.out.println(list2.size());
//		 for (org.apache.lucene.document.Document d : list2) {
//		 System.out.println(d.get("ID") + ":" + d.get("page"));
//		 }

		LuceneSearchService luceneSearchService = new LuceneSearchService();
		String title_keyword = "慕课", body_keyword = "课程";
		Integer title_body = 0;
		Long uploadtimeF = Long.MIN_VALUE, uploadtimeT = Long.MAX_VALUE;
		String[] types = null;
		Integer[] cates = null;
		Integer[] roleids = null;
		luceneSearchService.search(title_keyword, body_keyword, title_body,
				uploadtimeF, uploadtimeT, types, cates, roleids, 1l, 10l);

	}
}
