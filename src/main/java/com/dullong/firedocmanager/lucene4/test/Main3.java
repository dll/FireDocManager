package com.dullong.firedocmanager.lucene4.test;

import java.io.IOException;
import java.util.List;

import com.dullong.firedocmanager.bean.PDocument;
import com.dullong.firedocmanager.lucene4.LuceneIndexService;
import com.dullong.firedocmanager.lucene4.LuceneSearchService;
import com.dullong.firedocmanager.lucene4.domain.QDocument;
import com.mysema.query.lucene.LuceneQuery;

public class Main3 {

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		LuceneIndexService luceneIndexService = new LuceneIndexService();
		QDocument doc = new QDocument("doc");

		LuceneQuery query = new LuceneQuery(
				luceneIndexService.getIndexSearcher());

		List<org.apache.lucene.document.Document> list2;
//		= query.where(
//				doc.ID.eq(5l), null).list();
//
//		System.out.println(query.toString());
//		System.out.println(list2.size());
//		for (org.apache.lucene.document.Document d : list2) {
//			System.out.println(d.get("ID") + ":" + d.get("page"));
//		}

		System.out.println("-------------------------");
		list2 = query.where(
				doc.body.contains("课程")).list();
		
		System.out.println(query.toString());
		System.out.println(list2.size());
		for (org.apache.lucene.document.Document d : list2) {
			System.out.println(d.get("ID") + ":" + d.get("page"));
		}

	}

}
