package com.dullong.firedocmanager.lucene4.domain;

import com.dullong.firedocmanager.bean.Document;
import com.mysema.query.types.PathMetadataFactory;
import com.mysema.query.types.path.EntityPathBase;
import com.mysema.query.types.path.NumberPath;
import com.mysema.query.types.path.StringPath;

public class QDocument extends EntityPathBase<Document> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public QDocument(String var) {
		super(Document.class, PathMetadataFactory.forVariable(var));
	}

	public final StringPath title = createString("title");
	public final StringPath type = createString("type");
	public final StringPath body = createString("body");

	public final NumberPath<Long> ID = createNumber("ID", Long.class);
	public final NumberPath<Long> uploadtime = createNumber("uploadtime",
			Long.class);
	public final NumberPath<Integer> page = createNumber("page", Integer.class);
	public final NumberPath<Integer> cate = createNumber("cate", Integer.class);
	public final NumberPath<Integer> roleid = createNumber("roleid",
			Integer.class);

	public static void main(String[] s) {

	}
}
