package com.dullong.firedocmanager.JODConverter;

import java.io.File;

import com.artofsolving.jodconverter.DocumentFormat;

public interface Converter {

	public void convert(File inputFile, File outputFile);

	public void convert(File inputFile, DocumentFormat inputFormat,
			File outputFile, DocumentFormat outputFormat);

}
