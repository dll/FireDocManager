package com.dullong.firedocmanager.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import com.dullong.firedocmanager.bean.Document;
import com.dullong.firedocmanager.bean.PageBean;
import com.dullong.firedocmanager.lucene4.Searchable;

public interface DocumentService {

	/**
	 * 获取指定ID的文档下载地址
	 * 
	 * @param type
	 *            文档类型
	 * 
	 * @param docid
	 * @return
	 */
	public String getDownUrl(Long docid, String type);

	/**
	 * 
	 * @return
	 */
	public Long getIndexNum();

	/**
	 * 根据文档ID获取文档内容
	 * 
	 * @param ID
	 * @return
	 */
	public Document get(Long ID);

	/**
	 * 转换指定的文档格式
	 * 
	 * @param ID
	 * @return
	 */
	public boolean convert(Long ID);

	/**
	 * 判断是否已经转换为pdf格式
	 * 
	 * @param ID
	 * @return
	 */
	public boolean isConvert(Long ID);

	/**
	 * 处理指定ID的文档的文本，并存入数据库中。
	 * 
	 * @param docID
	 */
	public boolean parserBody(Long docID);

	/**
	 * 修改某个文档的标题和分类。注：如果为null则不修改
	 * 
	 * @param docId
	 * @param title
	 * @param type
	 * @return
	 */
	public boolean alterTitle(Long docId, String title, Integer cate);

	/**
	 * 解析body为null的，且已经存在pdf格式的文档的文本，并存入数据库。
	 */
	public void parserAll();

	/**
	 * 获取所有需要提取内容的文档id
	 * 
	 * @return
	 */
	public List<Long> getAllNeedParse();

	/**
	 * 审核一些文档
	 * 
	 * @param docIds
	 */
	public boolean shenhe(Integer zhuangtai, Long... docIds);

	public boolean shenheAll(Integer zhuangtai);

	/**
	 * 将指定文档进行索引
	 * 
	 * @param docIds
	 */
	public void indexDoc(Long... docIds);

	/**
	 * 索引为索引的文档
	 */
	public void indexAll() throws SQLException, IOException,
			ClassNotFoundException;

	/**
	 * 重建索引 1.删除索引2.重新构建索引
	 * 
	 * @throws SQLException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public void reIndexAll() throws SQLException, IOException,
			ClassNotFoundException;

	/**
	 * 优化索引
	 * 
	 * @param obj
	 */
	public void indexPptimize(Class<? extends Searchable> obj);

	/**
	 * 删除一些文档，并且从索引中移除
	 * 
	 * @param docIds
	 */
	public boolean delete(Long... docIds);

	/**
	 * 判断某个文档是否已经被索引。
	 * 
	 * @param docId
	 * @return
	 */
	public boolean isIndex(Long docId);

	/**
	 * 查询
	 * 
	 * @param page
	 * @param pnum
	 * @param searchword
	 * @return
	 */
	public List<Document> listAll(Long page, Long pnum, String searchword);

	public PageBean getPage(Long page, Long pnum, String searchword);

	/**
	 * 更新docid对应的文档的信息
	 * 
	 * @param docId
	 * @param docname
	 * @param cate
	 * @param roleid
	 * @return
	 */
	public boolean update(Long docId, String docname, Integer cate,
			Integer roleid);

}
