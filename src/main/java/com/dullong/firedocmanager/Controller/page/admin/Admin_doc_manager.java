package com.dullong.firedocmanager.Controller.page.admin;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.dullong.firedocmanager.bean.Cate;
import com.dullong.firedocmanager.bean.Document;
import com.dullong.firedocmanager.bean.PageBean;
import com.dullong.firedocmanager.service.CateService;
import com.dullong.firedocmanager.service.DocumentService;
import com.dullong.firedocmanager.util.json.SendJsonUtil;

@Controller
public class Admin_doc_manager {

	@Autowired
	DocumentService documentService;
	@Autowired
	CateService cateService;

	@RequestMapping(value = { "/admin/doc_manager.html",
			"/admin/doc_search.html" })
	public ModelAndView getAllDocs(
			HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "page", required = false) Long page,
			@RequestParam(value = "pnum", required = false) Long pnum,
			@RequestParam(value = "searchword", required = false) String searchword) {
		ModelAndView view = new ModelAndView();
		page = page == null ? 1 : page;
		pnum = pnum == null ? 10 : pnum;
		searchword = null == searchword ? "" : searchword;

		List<Document> listAll = documentService
				.listAll(page, pnum, searchword);
		view.addObject("docs", listAll);
		PageBean page2 = documentService.getPage(page, pnum, searchword);
		view.addObject("page", page2);

		view.addObject("searchword", searchword);

		view.setViewName("admin/docmanager");
		return view;
	}

	@RequestMapping(value = { "/admin/doc_shenhe.html" })
	public void shenhe(
			HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "shenhe", required = true) Boolean shenhe,
			@RequestParam(value = "shenheall", required = true) Boolean shenheall,
			@RequestParam(value = "id", required = false) Long id) {
		Integer zhuangtai = shenhe == true ? 1 : 0;
		shenheall = shenheall == true ? true : false;
		boolean success = false;
		if (!shenheall) {
			success = documentService.shenhe(zhuangtai, id);
		} else {
			success = documentService.shenheAll(zhuangtai);
		}

		SendJsonUtil.sendJson("{\"success\":" + success + "}", response);
	}

	@RequestMapping(value = { "/admin/doc_index.html" })
	public void index(
			HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "index", required = true) Boolean index,
			@RequestParam(value = "indexall", required = true) Boolean indexall,
			@RequestParam(value = "id", required = false) Long id) {
		boolean success = false;
		try {
			if (indexall)
				documentService.indexAll();
			else
				documentService.indexDoc(id);
			success = true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SendJsonUtil.sendJson("{\"success\":" + success + "}", response);
	}

	/**
	 * 重建索引
	 * 
	 * @param request
	 * @param response
	 * @param index
	 * @param indexall
	 * @param id
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @throws SQLException
	 */
	@RequestMapping(value = { "/admin/doc_reindexall.html" })
	public void reIndexAll(HttpServletRequest request,
			HttpServletResponse response) throws SQLException, IOException,
			ClassNotFoundException {
		boolean success = true;
		documentService.reIndexAll();
		SendJsonUtil.sendJson("{\"success\":" + success + "}", response);
	}

	@RequestMapping(value = { "/admin/doc_add.html" })
	public ModelAndView docAddPage(HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView view = new ModelAndView();
		List<Cate> listAll = cateService.listAll();
		view.addObject("allcates", listAll);
		view.setViewName("admin/doc_upload");
		return view;
	}

	@RequestMapping("/admin/doc_updateMeta.html")
	public void docAddMeta(HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "docId", required = true) Long docId,
			@RequestParam(value = "docname", required = true) String docname,
			@RequestParam(value = "cate", required = true) Integer cate,
			@RequestParam(value = "role", required = true) Integer role) {

		boolean success = documentService.update(docId, docname, cate, role);
		SendJsonUtil.sendJson("{\"success\":" + success + "}", response);

	}

	@RequestMapping("/admin/doc_del.html")
	public void docDel(HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "docId", required = true) Long docId) {

		boolean success = documentService.delete(docId);
		SendJsonUtil.sendJson("{\"success\":" + success + "}", response);

	}

}
