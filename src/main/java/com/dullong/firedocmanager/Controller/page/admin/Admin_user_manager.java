package com.dullong.firedocmanager.Controller.page.admin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.dullong.firedocmanager.bean.Role;
import com.dullong.firedocmanager.bean.User;
import com.dullong.firedocmanager.service.RoleService;
import com.dullong.firedocmanager.service.UserService;
import com.dullong.firedocmanager.util.json.SendJsonUtil;

@Controller
public class Admin_user_manager {

	@Autowired
	UserService userService;
	@Autowired
	RoleService roleService;

	@RequestMapping(value = { "/admin/user_manager.html" })
	public ModelAndView getAllUsers(HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView view = new ModelAndView();
		List<User> allUsers = userService.getAllUsers();
		view.addObject("users", allUsers);
		view.setViewName("admin/usermanager");
		return view;
	}

	@RequestMapping(value = { "/admin/user_manager_quickadd.html" })
	public ModelAndView quickAddUsers(HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView view = new ModelAndView();
		List<Role> listByParent = roleService.listRootRole();
		processRoleTree(listByParent);
		view.addObject("roletree", listByParent);
		view.setViewName("admin/usermanager_quickadd");
		return view;
	}

	@RequestMapping(value = { "/admin/adduser.html" })
	public ModelAndView quickAddUser(HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "userName", required = true) String userName,
			@RequestParam(value = "email", required = true) String email,
			@RequestParam(value = "roles", required = true) Integer... roles) {
		ModelAndView view = new ModelAndView();
		User u = new User();
		u.setUserName(userName);
		u.setEmail(email);
		u.setPSW("123");
		userService.save(u, roles);
		view.setViewName("redirect:/admin/user_manager_quickadd.html");
		return view;
	}

	@RequestMapping("/admin/user_authority_update.html")
	public void updateAuthorityByUser(
			HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "username", required = false) String username,
			@RequestParam(value = "roleid", required = false) Integer roleid,
			@RequestParam(value = "add", required = false) Boolean add) {
		boolean success;
		if (add) {
			success = userService.addRole(username, roleid);
		} else {
			success = userService.removeRole(username, roleid);
		}
		SendJsonUtil.sendJson("{\"success\":" + success + "}", response);
	}

	@RequestMapping(value = { "/admin/user_del.html" })
	public void userDel(HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "username", required = true) String username) {
		boolean success = userService.delete(username);
		SendJsonUtil.sendJson("{\"success\":" + success + "}", response);
	}

	@RequestMapping(value = { "/admin/user_edit_simple.html" })
	public void userEdit(
			HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "username", required = true) String username,
			@RequestParam(value = "email", required = true) String email,
			@RequestParam(value = "nickname", required = true) String nickname,
			@RequestParam(value = "y", required = true) Integer y,
			@RequestParam(value = "m", required = true) Integer m,
			@RequestParam(value = "d", required = true) Integer d,
			@RequestParam(value = "province", required = false) String province,
			@RequestParam(value = "city", required = false) String city,
			@RequestParam(value = "gender", required = true) Integer gender) {

		String updateString = userService
				.update("update user set email=?,nickName=?,year=?,month=?,day=?,sex=?,province=?,city=? where username=?",
						email, nickname, y, m, d, gender, province, city,
						username);

		SendJsonUtil.sendJson("{\"success\":\"" + updateString + "\"}",
				response);
	}

	@RequestMapping(value = { "/admin/user_edit_chpwd.html" })
	public void userEdit_chpwd(HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "username", required = true) String username,
			@RequestParam(value = "psw_old", required = true) String psw_old,
			@RequestParam(value = "psw", required = true) String psw) {
		String updateString;
		User userByUsername = userService.getUserByUsername(username);
		if (userByUsername != null && psw_old.equals(userByUsername.getPSW())) {
			updateString = userService.update(
					"update user set psw=? where username=?", psw, username);
		} else {
			updateString = "旧密码输入错误!";
		}

		SendJsonUtil.sendJson("{\"success\":\"" + updateString + "\"}",
				response);
	}

	private void processRoleTree(List<Role> listByParent) {
		if (listByParent != null) {
			for (Role c : listByParent) {
				Integer id = c.getId();
				List<Role> listByParent2 = roleService.listByParent(id);
				c.setChilds(listByParent2);
				processRoleTree(listByParent2);
			}
		}
	}
}
