package com.dullong.firedocmanager.Controller.page;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.dullong.firedocmanager.service.DocumentService;
import com.dullong.firedocmanager.util.OptionUtil;

@Controller
public class PdfShow {

	@Autowired
	DocumentService documentService;

	@RequestMapping(value = "/view/doc-{docid}.html", method = RequestMethod.GET)
	public ModelAndView show(@PathVariable Long docid) {
		ModelAndView view = new ModelAndView();
		String downUrl = documentService.getDownUrl(docid, "pdf");
		downUrl = FilenameUtils.normalize(downUrl, true);
		view.setViewName("redirect:/static/pdfview/web/viewer.html?file="+OptionUtil.getOption("file_server_prefix")+"/"+downUrl);
		return view;
	}

}
