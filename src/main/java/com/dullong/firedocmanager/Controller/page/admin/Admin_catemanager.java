package com.dullong.firedocmanager.Controller.page.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.dullong.firedocmanager.bean.Cate;
import com.dullong.firedocmanager.service.CateService;
import com.dullong.firedocmanager.util.json.JsonUtils;
import com.dullong.firedocmanager.util.json.SendJsonUtil;

@Controller
public class Admin_catemanager {

	@Autowired
	CateService cateService;

	@RequestMapping("/admin/cate.html")
	public ModelAndView listAllCate(HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView view = new ModelAndView();
		List<Cate> listByParent = cateService.listRootCate();
		List<Cate> listAll = cateService.listAll();
		view.addObject("catelist", listAll);
		processCateTree(listByParent);
		view.addObject("catetree", listByParent);
		view.setViewName("admin/fenlei");

		return view;
	}

	@RequestMapping("/admin/addcate.html")
	public ModelAndView addCate(HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "id", required = false) Integer id,
			@RequestParam(value = "parent", required = false) Integer parent,
			@RequestParam(value = "option", required = true) Integer option,
			@RequestParam(value = "title", required = true) String title) {
		ModelAndView view = new ModelAndView();
		if (option == 0) {
			parent = parent == -1 ? null : parent;
			cateService.save(title, parent);
		} else if (option == 1) {
			cateService.update(title, parent, id);
		} else if (option == 2) {
			cateService.delete(id);
		}
		view.setViewName("redirect:/admin/cate.html");
		return view;
	}

	@RequestMapping("/admin/cate-{id}.html")
	public void listCateByParent(HttpServletRequest request,
			HttpServletResponse response, @PathVariable Integer id) {
		List<Cate> listByParent = cateService.listByParent(id);
		JsonUtils jsonUtils = new JsonUtils();
		JsonUtils putPropertiy = jsonUtils.putPropertiy("data", listByParent);
		try {
			SendJsonUtil.sendJson(putPropertiy.writeToString(), response);
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void processCateTree(List<Cate> listByParent) {
		if (listByParent != null) {
			for (Cate c : listByParent) {
				Integer id = c.getId();
				List<Cate> listByParent2 = cateService.listByParent(id);
				c.setChilds(listByParent2);
				processCateTree(listByParent2);
			}
		}
	}
}
