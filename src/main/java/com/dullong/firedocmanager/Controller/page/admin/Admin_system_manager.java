package com.dullong.firedocmanager.Controller.page.admin;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.dullong.firedocmanager.util.OptionUtil;
import com.dullong.firedocmanager.util.json.SendJsonUtil;

@Controller
public class Admin_system_manager {

	/**
	 * 更新设置信息
	 * 
	 * @param request
	 * @param response
	 * @param file_server_prefix
	 * @param idx_path
	 * @param opload_filePath
	 */
	@RequestMapping("/admin/systemsetting.html")
	public void updateSetting(
			HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "file_server_prefix", required = false) String file_server_prefix,
			@RequestParam(value = "idx_path", required = false) String idx_path,
			@RequestParam(value = "opload_filePath", required = false) String opload_filePath) {
		if (!StringUtils.isEmpty(file_server_prefix)) {
			OptionUtil.saveOrUpdateOption("file_server_prefix",
					file_server_prefix);
		}
		if (!StringUtils.isEmpty(idx_path)) {
			OptionUtil.saveOrUpdateOption("idx_path", idx_path);
		}
		if (!StringUtils.isEmpty(opload_filePath)) {
			OptionUtil.saveOrUpdateOption("opload_filePath", opload_filePath);
		}
		boolean success = true;
		SendJsonUtil.sendJson("{\"success\":" + success + "}", response);
	}

	@RequestMapping("/admin/SystemSetting.html")
	public ModelAndView systemSettingPage() {
		ModelAndView view = new ModelAndView();

		List<Map<String, Object>> all = OptionUtil.getAll();
		for (Map<String, Object> x : all) {
			String attributeName = (String) x.get("option_name");
			String attributeValue = (String) x.get("option_value");
			view.addObject(attributeName, attributeValue);
		}
		view.setViewName("admin/SystemSetting");

		return view;
	}
}
