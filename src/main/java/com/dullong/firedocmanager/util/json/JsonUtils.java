package com.dullong.firedocmanager.util.json;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.ser.FilterProvider;
import org.codehaus.jackson.map.ser.impl.SimpleBeanPropertyFilter;
import org.codehaus.jackson.map.ser.impl.SimpleFilterProvider;

public class JsonUtils {
	private ObjectMapper om = new ObjectMapper();
	private Map<Object, Object> map = new HashMap<Object, Object>();

	public JsonUtils() {
		om.disable(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES);
		om.configure(SerializationConfig.Feature.FAIL_ON_EMPTY_BEANS, false);
	}

	/**
	 * 新增属性
	 * 
	 * @param name
	 * @param Value
	 */
	public JsonUtils putPropertiy(String name, Object value) {
		map.put(name, value);
		return this;
	}

	public JsonUtils addFilter(String filterName, String... properties) {
		FilterProvider filterProvider = new SimpleFilterProvider().addFilter(
				filterName,
				SimpleBeanPropertyFilter.serializeAllExcept(properties));
		om.setFilters(filterProvider);
		return this;
	}

	/**
	 * 将属性返回
	 * 
	 * @return
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonGenerationException
	 */
	public String writeToString() throws JsonGenerationException,
			JsonMappingException, IOException {
		return om.writeValueAsString(map);
	}

}
