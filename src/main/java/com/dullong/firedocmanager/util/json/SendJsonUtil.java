package com.dullong.firedocmanager.util.json;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

public class SendJsonUtil {
	
	@SuppressWarnings("unused")
	public static  void sendJson(String jsonString, HttpServletResponse response) {
		try {
			response.setCharacterEncoding("utf-8");
			response.setContentType("text/html;charset=utf-8");
			response.getWriter().print(jsonString);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
