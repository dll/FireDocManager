package com.dullong.firedocmanager.util;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.mchange.v2.c3p0.ComboPooledDataSource;

public class ConnectionUtil {

	private static DataSource ds;

	static {
		ds = new ComboPooledDataSource();
	}

	public static synchronized final Connection getConnection() {
		try {
			return ds.getConnection();	
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
