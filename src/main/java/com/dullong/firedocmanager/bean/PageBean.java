package com.dullong.firedocmanager.bean;

public class PageBean {

	private Long page;
	private Long pnum;
	private Long count;

	public PageBean(Long l, Long limit, Long count) {
		this.page = l;
		this.pnum = limit;
		this.count = count;
	}

	public Long getPage() {
		return page;
	}

	public void setPage(Long page) {
		this.page = page;
	}

	public Long getPnum() {
		return pnum;
	}

	public void setPnum(Long pnum) {
		this.pnum = pnum;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

}
