package com.dullong.firedocmanager.bean;

import java.util.List;

public class Cate {

	private Integer id;
	private String title;
	private Integer parent;
	private List<Cate> childs;//直属儿子的分类

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getParent() {
		return parent;
	}

	public void setParent(Integer parent) {
		this.parent = parent;
	}

	public List<Cate> getChilds() {
		return childs;
	}

	public void setChilds(List<Cate> childs) {
		this.childs = childs;
	}

}
